# Package

version       = "0.1.0"
author        = "skyvaultgames"
description   = "Game oriented math library for the Nim programming language."
license       = "MIT"

# Dependencies

requires "nim >= 0.17.2"

